# Chatty for macOS

This is [Chatty](https://github.com/chatty/chatty) for macOS.

## Downloads

Head over to the [Releases](https://gitlab.com/dehesselle/chatty_macos/-/releases) section to find downloads for both Intel and Apple Silicon.

## License

This work is licensed under [GPL-3.0-or-later](LICENSE).

Quoting [Chatty's README.md](https://github.com/chatty/chatty#readme):

> Chatty, as a whole, is released under the GPLv3 or later (see included [LICENSE](https://github.com/chatty/chatty/blob/master/LICENSE) file).
